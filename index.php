<?php get_header(); ?>
<div class="page-banner">
    <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('images/ocean.jpg'); ?> );"></div>
    <div class="page-banner__content container container--narrow">
      <h1 class="page-banner__title">Welcome To Our Blog</h1>
      <div class="page-banner__intro">
        <p>Keep Updated with Our latest Post.</p>
      </div>
    </div>  
  </div>
  <div class="container container--narrow page-section">
  	<?php while ( have_posts() ) : the_post(); ?><!--if true goes for every single post -->
  		<div class="post-item">
  			<h2 class="headline headline--medium headline--post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  			<div class="metabox">
  				<!-- totally dynamic gives the author link , time of creation and the category list -->
  				posted By <?php the_author_posts_link(); ?> on <?php the_time('n.j.y'); ?> in <?php echo get_the_category_list(','); ?>
  			</div>
  			<div class="generic-content">
  				<?php the_excerpt(); ?><!-- does not give all the content -->
  				<p><a class="btn btn--blue" href="<?php the_permalink(); ?>">Continue Reading&raquo;</a></p>
  			</div>
  			
  		</div>
  	<?php endwhile ?>
  	<?php echo paginate_links(); ?><!-- gives pagination -->
  </div>	
<?php get_footer(); ?>