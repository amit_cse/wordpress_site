<?php get_header(); ?>
    <?php while ( have_posts() ) : the_post(); ?>
   	<div class="page-banner">
   	    <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('images/ocean.jpg'); ?> );"></div>
   	    <div class="page-banner__content container container--narrow">
   	      <h1 class="page-banner__title"><?php the_title(); ?></h1>
   	      <div class="page-banner__intro">
   	        <p>Don't forget to replace me later.</p>
   	      </div>
   	    </div>  
   	  </div>
   	  <div class="container container--narrow page-section">
   	  	<div class="metabox">
   	  	posted By <?php the_author_posts_link(); ?> on <?php the_time('n.j.y'); ?> in <?php echo get_the_category_list(','); ?>
   	  	</div>
   	  	<div class="generic-content">
   	  		<?php the_content(); ?>
   	  	</div>
   	  </div>	
   <?php endwhile; ?>
<?php get_footer(); ?>   
   

 