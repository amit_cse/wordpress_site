<?php get_header(); ?>
<?php
  while (have_posts()) {
   	the_post(); ?>
    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('images/ocean.jpg'); ?> );"></div>
        <div class="page-banner__content container container--narrow">
          <h1 class="page-banner__title"><?php the_title(); ?></h1>
          <div class="page-banner__intro">
            <p>Don't forget to replace me later.</p>
          </div>
        </div>  
      </div>

      <div class="container container--narrow page-section">
      	<!--Import method listed below:"get_the_ID()-> return the id of the current page" -->
      	<!-- wp_get_post_parent_id() -> return the id of the parent -->
      	<?php $theParent = wp_get_post_parent_id(get_the_ID()); echo $theParent; ?>
      	<?php  if ($theParent) : ?>
      		<div class="metabox metabox--position-up metabox--with-home-link">
      		  <p><a class="metabox__blog-home-link" href="<?php echo get_permalink($theParent); ?>"><i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo get_the_title($theParent); ?></a> <span class="metabox__main"><?php the_title(); ?></span></p>
      		  <!-- "the_title() method return the title of the current page " -->
      		</div>
      	    	
      	 <?php endif; ?>   
      	  

        
        
        <div class="page-links">
          <h2 class="page-links__title"><a href="#">About Us</a></h2>
          <ul class="min-list">
            <?php
            if ($theParent) {
             	$findChildrenOf = $theParent;
             } else {
             	$findChildrenOf = get_the_ID();
             }

                 wp_list_pages( array(
            	'title_li' => NULL,
            	'child-of' => $findChildrenOf
            ));
             ?>
           </ul>  
        </div>

        <div class="generic-content">
         <?php the_content(); ?>
        </div>

      </div>
 
   <?php } 
 ?> 
<?php get_footer(); ?>
 