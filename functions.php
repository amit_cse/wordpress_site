<?php
//function to load the rquire style scripts or file 
function universityFiles() {
	wp_enqueue_script('main-university-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
	wp_enqueue_style('custom-google-fonts','//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
	wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('university_main_style', get_stylesheet_uri(), NULL, microtime());
}
add_action('wp_enqueue_scripts', 'universityFiles'); 

//function to load the  title of the page.. and that is dynamic
function customtheme_setup() {
   	//register a custom primary navigation menu
   	register_nav_menu('headerMenuLocation', 'Header Menu Location');
   	register_nav_menu('footerLocationOne', 'Footer Location One');
   	register_nav_menu('footerLocationTwo', 'Footer Location Two');
   	//gives the dynamic title tag
   	add_theme_support( 'title-tag' );
   } 
   add_action( 'after_setup_theme', 'customtheme_setup' );

 ?>
